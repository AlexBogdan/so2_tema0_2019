if test "$1" = "check"
then
    cd skels/api_assignment/kernel-api/
    cp ../checker/list-checker checker
    chmod 777 checker
    ./checker
    cd ~
fi

if test "$1" = "interactive"
then
    insmod skels/api_assignment/kernel-api/list.ko
    echo -e "Enter your commands | Stop keyword: 'end' | Preview keyword: 'preview' \n"
    while true
    do
        read -p "CMD: " cmd

        if test "$cmd" = "preview"
        then
            cat /proc/list/preview
        fi

        if test "$cmd" = "end"
        then
            break
        fi

        echo "$cmd" > /proc/list/management
    done
    rmmod skels/api_assignment/kernel-api/list.ko
fi
