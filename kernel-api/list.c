// SPDX-License-Identifier: DPS-1.0 (Doar Pentru Smecheri)
/*
 * list.c - Linux kernel list API
 *
 * Author: Bogdan-Alexandru Andrei <alex.bogdan.acs@gmail.com>
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/uaccess.h>

#define PROCFS_MAX_SIZE		1000

#define procfs_dir_name		"list"
#define procfs_file_read	"preview"
#define procfs_file_write	"management"

struct proc_dir_entry *proc_list;
struct proc_dir_entry *proc_list_read;
struct proc_dir_entry *proc_list_write;

// Definim lista in care sa putem stoca texte
struct text_list {
	char *text;
	struct list_head list;
};

// Initialiam lista de texte
LIST_HEAD(texts);
// Vom pastra mereu un pointer catre ultimul nod pentru 'adde'
struct list_head *last_element;

// Functia care adauga textul la inceputul listei
static int addf(char *text)
{
	// Alocam un nod al listei in care salvam textul primit
	struct text_list *node = kmalloc(sizeof(*node), GFP_KERNEL);

	if (!node)
		return -ENOMEM;

	// Salvam textul si adaugam noul nod in lista de texte
	node->text = kmalloc(strlen(text), GFP_KERNEL);
	strcpy(node->text, text);
	list_add(&node->list, &texts);

	if (last_element == NULL)
		last_element = &(node->list);

	return 0;
}

// Functia care adauga textul la finalul listei
static int adde(char *text)
{
	struct text_list *node;

	//	In cazul in care lista este goala, vom adauga in fata,
	// neavand acest pointer disponibil
	if (last_element == NULL)
		return addf(text);

	// Alocam un nod al listei in care salvam textul primit
	node = kmalloc(sizeof(*node), GFP_KERNEL);

	if (!node)
		return -ENOMEM;

	// Salvam textul si adaugam noul nod in lista de texte
	node->text = kmalloc(strlen(text), GFP_KERNEL);
	strcpy(node->text, text);
	list_add(&node->list, last_element);

	// Actualizam pointerul catre ultimul element din lista
	last_element = &(node->list);

	return 0;
}

// Functia care sterge prima aparitie a textului 'text' din lista
static int delf(char *text)
{
	// Alocam un nod al listei in care salvam textul primit
	struct list_head *i = NULL, *tmp = NULL, *prev = NULL;
	struct text_list *node = NULL;

	list_for_each_safe(i, tmp, &texts) {
		node = list_entry(i, struct text_list, list);
		if (strcmp(node->text, text) == 0) {
			if (i == last_element)
				last_element = prev;

			list_del(i);
			kfree(node);
			return 0;
		}
		prev = i;
	}

	// Nu am gasit textul in lista
	return -EINVAL;
}

// Functia care distruge o lista definitiv
static void destroy_list(void)
{
	struct list_head *i = NULL, *n = NULL;
	struct text_list *node = NULL;

	list_for_each_safe(i, n, &texts) {
		node = list_entry(i, struct text_list, list);
		list_del(i);
		kfree(node->text);
		kfree(node);
	}
}

static int list_proc_show(struct seq_file *m, void *v)
{
	struct text_list *node = NULL;

	list_for_each_entry(node, &texts, list) {
		seq_puts(m, node->text);
		seq_puts(m, "\n");
	}

	return 0;
}

static int list_read_open(struct inode *inode, struct  file *file)
{
	return single_open(file, list_proc_show, NULL);
}

static int list_write_open(struct inode *inode, struct  file *file)
{
	return single_open(file, list_proc_show, NULL);
}

// Functia care verifica tipul unei comenzi primite
static int command_type(char *cmd)
{
	if (strstr(cmd, "addf") == cmd)
		return 1;
	if (strstr(cmd, "adde") == cmd)
		return 2;
	if (strstr(cmd, "delf") == cmd)
		return 3;
	if (strstr(cmd, "dela") == cmd)
		return 4;

	return -1;
}

static ssize_t list_write(struct file *file, const char __user *buffer,
			  size_t count, loff_t *offs)
{
	char local_buffer[PROCFS_MAX_SIZE];
	unsigned long local_buffer_size = 0;
	char *text;

	local_buffer_size = count;
	if (local_buffer_size > PROCFS_MAX_SIZE)
		local_buffer_size = PROCFS_MAX_SIZE;

	memset(local_buffer, 0, PROCFS_MAX_SIZE);
	if (copy_from_user(local_buffer, buffer, local_buffer_size))
		return -EFAULT;

	// Scapam de acest newline adaugat aiurea de echo
	if (local_buffer[strlen(local_buffer) - 1] == '\n')
		local_buffer[strlen(local_buffer) - 1] = '\0';

	// Obtinem doar cuvantul care va trebui adaugat
	text = strchr(local_buffer, ' ');

	// Verificam ce tip de comanda avem si o aplicam
	switch (command_type(local_buffer)) {
	case 1:
		addf(text + 1);
		break;
	case 2:
		adde(text + 1);
		break;
	case 3:
		delf(text + 1);
		break;
	case 4:
		while (delf(text + 1) != -EINVAL)
			;
		break;
	}

	return local_buffer_size;
}

static const struct file_operations r_fops = {
	.owner		= THIS_MODULE,
	.open		= list_read_open,
	.read		= seq_read,
	.release	= single_release,
};

static const struct file_operations w_fops = {
	.owner		= THIS_MODULE,
	.open		= list_write_open,
	.write		= list_write,
	.release	= single_release,
};

static int list_init(void)
{
	proc_list = proc_mkdir(procfs_dir_name, NULL);
	if (!proc_list)
		return -ENOMEM;

	proc_list_read = proc_create(procfs_file_read, 0000, proc_list,
				     &r_fops);
	if (!proc_list_read)
		goto proc_list_cleanup;

	proc_list_write = proc_create(procfs_file_write, 0000, proc_list,
				      &w_fops);
	if (!proc_list_write)
		goto proc_list_read_cleanup;

	return 0;

proc_list_read_cleanup:
	proc_remove(proc_list_read);
proc_list_cleanup:
	proc_remove(proc_list);
	return -ENOMEM;
}

static void list_exit(void)
{
	proc_remove(proc_list);
	destroy_list();
}

module_init(list_init);
module_exit(list_exit);

MODULE_DESCRIPTION("Linux kernel list API");
MODULE_AUTHOR("Bogdan-Alexandru Andrei <alex.bogdan.acs@gmail.com>");
MODULE_LICENSE("GPL v2");
